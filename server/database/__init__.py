from flask import Flask
from datetime import datetime
from flask_pymongo import PyMongo

from server.config import settings

app = Flask(__name__,  static_folder='../../web/static', template_folder='../../web/templates')

app.config["MONGO_URI"] = settings.MONGO_URI
    
mongo = PyMongo(app)
db=mongo.db