from datetime import datetime
from flask import Blueprint, render_template, request, redirect
from bson.objectid import ObjectId

from server.database import db

router = Blueprint('routes', __name__, static_folder='../web/static', template_folder='../web/templates')

@router.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        body= {
            'content': request.form['content'],
            'date_created': datetime.utcnow()
        }
        db.todos.insert_one(body)

        try:
            return redirect('/')
        except:
            return 'There was an issue adding your task'

    else:
        tasks = [doc for doc in db.todos.find().sort('date_created', -1)]
        return render_template('index.html', tasks=tasks)


@router.route('/delete/<string:id>')
def delete(id):
    try:
        db.todos.delete_one({'_id': ObjectId(id)})
        return redirect('/')
    except:
        return 'There was a problem deleting that task'

@router.route('/update/<string:id>', methods=['GET', 'POST'])
def update(id):
    if request.method == 'POST':
        newContent = request.form['content']

        try:
            db.todos.update_one({'_id': ObjectId(id)}, {'$set': {'content': newContent}})
            return redirect('/')
        except Exception as err:
            print('Error', err)
            return 'There was an issue updating your task'

    else:
        for task in db.todos.find({'_id': ObjectId(id)}):
            return render_template('update.html', task=task)