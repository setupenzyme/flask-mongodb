import os
from typing import Any, Dict, Optional

from pydantic import BaseSettings, MongoDsn, validator

class Settings(BaseSettings):
    MONGO_SERVER: str = os.environ["MONGO_SERVER"]
    MONGO_USER: str = os.environ["MONGO_USER"]
    MONGO_PASSWORD: str = os.environ["MONGO_PASSWORD"]
    MONGO_DB: str = os.environ["MONGO_DB"]
    DB_PORT: int = int(os.environ.get("DB_PORT", 27017))
    MONGO_URI: Optional[MongoDsn] = f"mongodb://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_SERVER}:{DB_PORT}/{MONGO_DB}?retryWrites=true&w=majority&authSource=admin"

    # @validator("MONOG_URI", pre=True)
    # def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
    #     if isinstance(v, str):
    #         return v
    #     return MongoDsn.build(
    #         scheme="mongo",
    #         user=values.get("MONGO_USER"),
    #         password=values.get("MONGO_PASSWORD"),
    #         host=values.get("MONGO_SERVER"),
    #         path=f"/{values.get('MONGO_DB') or ''}",
    #     )

    class Config:
        case_sensitive = True


settings = Settings()
